﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ccbl.Models;

namespace ccbl
{
    public class applyModel : PageModel
    {
        [BindProperty]
        public ApplyModel Apply { get; set; }

  
        public void OnGet()

        {

        }

        public IActionResult OnPost()
        {
            if (ModelState.IsValid == false)
            {
                return Page();
            }
            // getting values from apply page and then posting it to confirmation page
            return RedirectToPage("Confirmation", new { Apply.ExtraInfo, Apply.Email, Apply.Name, Apply.Password} );

       }
    }
}