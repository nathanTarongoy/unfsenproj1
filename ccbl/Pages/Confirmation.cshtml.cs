﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ccbl
{
    public class ConfirmationModel : PageModel
    {
        [BindProperty(SupportsGet = true)]
        public string Name { get; set; }
        [BindProperty(SupportsGet = true)]
        public string Email { get; set; }
        [BindProperty(SupportsGet = true)]
        public string Password { get; set; }
        [BindProperty(SupportsGet = true)]
        public string ExtraInfo { get; set; }

        public void OnGet()
        {
           if (string.IsNullOrWhiteSpace(Name))
            {
                Name = "the web";
            }
        if (string.IsNullOrWhiteSpace(Email))
            {
                Email = "the web";
            }
             if (string.IsNullOrWhiteSpace(Password))
            {
                Password = "the web";
            }
            if (string.IsNullOrWhiteSpace(ExtraInfo))
            {
                ExtraInfo = "the web";
            }
            
        }

        public void OnPost()
        { }
    }
}